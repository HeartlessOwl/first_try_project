=== AyaCoffeeShop ===
Contributors: ayatemplates
Tags: blog, two-columns, right-sidebar, custom-logo, custom-background, custom-header,
custom-menu, featured-images, threaded-comments, translation-ready, sticky-post,
theme-options, footer-widgets, full-width-template, editor-style
Requires at least: 4.8.0
Tested up to: 4.9.5a
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

AyaCoffeeShop is Fully Responsive WordPress Theme.

== Description ==

AyaCoffeeShopPro is a fully responsive WordPress Theme. Features include a built-in Slider, Custom Logo, Multi-level Drop-down Menu,
Custom Background, Full-Width Page Template, Footer Widgets Areas, Footer Menu, Footer Copyright Text, Search Engine Optimized,
Multiple Browsers Support, Translation-Ready, etc. Fully Responsive, Customizable, and easy to use theme.


== Frequently Asked Questions ==

= How to Install the Theme? =

Manual installation:

1. Upload the theme folder to the /wp-content/themes/ directory

Installation using 'Add New Theme'

1. From your Admin UI (Dashboard), use the menu to select Themes -> Add New
2. Search for theme name
3. Click the 'Install' button to open the theme's repository listing
4. Click the 'Install' button

Activiation and Use

1. Activate the Theme through the 'Themes' menu in WordPress
2. See Appearance -> Customize to change theme specific options

== Changelog ==
= 1.0.2 =
* updating screenshot.png according to the new WordPress.org requirements

= 1.0.1 =
* display slider only on static homepage
* remove default demo content

= 1.0.0 =
* Initial Release

== Copyright ==

AyaCoffeeShop WordPress Theme, Copyright 2018 ayatemplates
AyaCoffeeShop is distributed under the terms of the GNU GPL

AyaCoffeeShop WordPress Theme bundles the following third-party resources:

== Resources ==

"Pro" theme section examples for the customizer, Copyright 2016 Justin Tadlock
"Pro" theme section examples for the customizer is licensed under the GNU GPL, version 2 or later.
Source: https://github.com/justintadlock/trt-customizer-pro

FontAwesome icon font, Copyright Font Awesome
FontAwesome are licensed under the terms of the SIL OFL 1.1 license.
Source: https://fontawesome.com/v4.7.0/

Screenshot Slider Image, Copyright Pexels
Image is licensed under the terms of the CC0 Creative Commons license.
Source: https://pixabay.com/en/coffee-cup-drink-flora-flower-hot-1845623/

images/slider/1.jpg Image, Copyright  Pexels
Image is licensed under the terms of the CC0 Creative Commons license.
Source: https://pixabay.com/en/coffee-cup-drink-flora-flower-hot-1845623/

images/slider/2.jpg Image, Copyright  gollosojov23
Image is licensed under the terms of the CC0 Creative Commons license.
Source: https://pixabay.com/en/coffee-cup-drink-espresso-hot-3348813/

images/slider/3.jpg Image, Copyright  Free-Photos
Image is licensed under the terms of the CC0 Creative Commons license.
Source: https://pixabay.com/en/coffee-cup-drink-espresso-caffeine-690054/
